NAME = mknb-c
MAIN = src/main.c
BUILDDIR = build
CFLAGS = -static -fuse-ld=mold -flto=16 -march=native -Wextra -Wall -Wfloat-equal -pedantic -Wswitch-enum -Werror -O3 -std=c2x -floop-nest-optimize -fgraphite-identity -floop-parallelize-all
#LDFLAGS= -O1 -shared -flto=16 -plugin
CC = gcc

all: dir lib_types lib_VM lib_VMAlloc libArrayList main
	$(CC) $(CFLAGS) $(BUILDDIR)/* -o $(NAME)
	strip $(NAME)

main: 
	$(CC) $(CFLAGS) -c $(MAIN) -o $(BUILDDIR)/main.o

lib_types:
	$(CC) $(CFLAGS) -c src/lib/types/types.c -o $(BUILDDIR)/types.o

lib_VM:
	$(CC) $(CFLAGS) -c src/lib/VM/VM.c -o $(BUILDDIR)/VM.o

lib_VMAlloc:
	$(CC) $(CFLAGS) -c src/lib/VMAlloc/VMAlloc.c -o $(BUILDDIR)/VMAlloc.o

libArrayList:
	$(CC) $(CFLAGS) -c src/lib/libArrayList/libArrayList.c -o $(BUILDDIR)/libArrayList.o


debug: CFLAGS+=-g 
debug: dir lib_types lib_VM lib_VMAlloc libArrayList main
	$(CC) $(CFLAGS) $(BUILDDIR)/* -o $(NAME)

dir:
	mkdir -p $(BUILDDIR)

clean:
	rm $(BUILDDIR)/*
