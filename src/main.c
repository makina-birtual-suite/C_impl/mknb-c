#include <stdio.h>
#include <stdlib.h>
#include "lib/types/types.h"
#include "lib/VM/VM.h"
#include "lib/libArrayList/libArrayList.h"


i32 main(i32 argc, i8 *argv[]) {
  for (i32 i = 0; i < argc; ++i) fprintf(stdout, "%s\n", argv[i]);
  ArrayList* test = ArrayList_new(8);
  u64 val = 2612;
  if (ArrayList_push(test, &val)) {
    ArrayList_dump_mem(test);
    u64 pop_res = 0; 
    if (ArrayList_pop(test, &pop_res)) {
      printf("popped: %lld \n", pop_res);
    }
  }  
  VM* vm = VM_new();
  
  printf("Hello Makina!\n");
  
  ArrayList_free(test);
  VM_free(vm);  
  return 0;
}
