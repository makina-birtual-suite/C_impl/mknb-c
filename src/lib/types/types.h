#ifndef TYPES_H
#define TYPES_H
#define u8  unsigned char
#define i8  char
#define i64 long long int 
#define u64 unsigned long long int 
#define i32 int 
#define u32 unsigned int
#define f64 double
#define f32 float
#endif // !TYPES_H
