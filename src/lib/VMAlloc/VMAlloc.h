#ifndef VMALLOC_H
#define VMALLOC_H
#include "../libArrayList/libArrayList.h"
#include <stdbool.h>
#include <stdlib.h>
#include "../types/types.h"

// ---------------------------------------------------
typedef struct vp VirtualPtr;
u64 VirtualPtr_id(const VirtualPtr* const vp);
u64 VirtualPtr_pos(const VirtualPtr* const vp);
u64 VirtualPtr_size(const VirtualPtr *const vp);
VirtualPtr* VirtualPtr_new(u64 virt, u64 raw, u64 size);
// ---------------------------------------------------
typedef struct va VmArena;
u64 VmArena_total_size(const VmArena* const va);
u64 VmArena_free_ptr(const VmArena* const va);
ArrayList* VmArena_memory(const VmArena* const va);
VmArena* VmArena_new(u64 total_size);
void VmArena_deinit(VmArena* va);
bool VmArena_resize(VmArena* va, u64 size); 
void VmArena_free(VmArena* a);
//----------------------------------------------------
typedef struct valloc VmAllocator;
VmAllocator* VmAllocator_new(u64 page_size);
bool VmAllocator_add_page(VmAllocator* valloc);
bool VmAllocator_get_raw_ptr(VmAllocator* v, u64 virt_ptr, u64* raw_ptr);
bool VmAllocator_get_min_virt_ptr(VmAllocator* v, u64* virt_ptr);
void VmAllocator_collect_garbage(VmAllocator* v);
void VmAllocator_free_pages(VmAllocator* v);
u64 VmAllocator_malloc(VmAllocator* v,u64 nbytes);
void VmAllocator_mfree(VmAllocator* v, u64 virt_ptr);
bool VmAllocator_size_of_alloc(VmAllocator *v, u64 virt_ptr, u64* size);
void VmAllocator_kill(VmAllocator* v);
#endif // !VMALLOC_H
