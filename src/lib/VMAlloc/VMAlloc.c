// yippee UwU :3 bestest boyfie ever -><-
#include "VMAlloc.h"
#include <stdlib.h>

//--------------------------------------------------------------
#define MAX_PAGES 10
//--------------------------------------------------------------
typedef struct vp {
  u64 id;
  u64 pos;
  u64 size;
} VirtualPtr;

u64 VirtualPtr_id(const VirtualPtr *const vp) {
  return vp->id;
}

u64 VirtualPtr_pos(const VirtualPtr *const vp) {
  return vp->pos;
}

u64 VirtualPtr_size(const VirtualPtr *const vp) {
  return vp->size;
}

VirtualPtr* VirtualPtr_new(u64 virt, u64 raw, u64 size) {
  VirtualPtr* res = malloc(sizeof(VirtualPtr));
  res->size = size;
  res->pos = raw;
  res->id = virt;
  return res;
}
//--------------------------------------------------------------
typedef struct va {
  u64 total_size;
  u64 free_ptr;
  ArrayList* memory;
} VmArena;
u64 VmArena_total_size(const VmArena* const va) {
  return va->total_size;
}
u64 VmArena_free_ptr(const VmArena* const va) {
  return va->free_ptr;
}
ArrayList* VmArena_memory(const VmArena* const va) {
  return va->memory;
}
VmArena* VmArena_new(u64 total_size) {
  VmArena* res = malloc(sizeof(VmArena));
  res->free_ptr = 0;
  res->total_size = total_size;
  res->memory = ArrayList_new(1);
  ArrayList_resize(res->memory, total_size, 0);
  return res;
}

void VmArena_deinit(VmArena* va) {
  va->free_ptr = 0;
}

bool VmArena_resize(VmArena* va, u64 size) {
  va->total_size = size;
  return ArrayList_resize(va->memory, size, 0);
}

void VmArena_free(VmArena *a) {
  free(a->memory);
  free(a);
}

//--------------------------------------------------------------
typedef struct valloc {
  u64 page_size;
  u64 page_number;
  ArrayList* alloc_blocks;
  VmArena* fst_arena;
} VmAllocator;

VmAllocator* VmAllocator_new(u64 page_size) {
  VmAllocator* res = malloc(sizeof(VmAllocator));
  res->page_size = page_size;
  res->page_number = 0;
  res->alloc_blocks = ArrayList_new(sizeof(VirtualPtr));
  res->fst_arena = VmArena_new(0);
  return res;
}

bool VmAllocator_add_page(VmAllocator *valloc) {
  valloc->page_number += 1;
  return VmArena_resize(valloc->fst_arena, valloc->page_number);
}

bool VmAllocator_get_raw_ptr(VmAllocator *v, u64 virt_ptr, u64* raw_ptr) {
  bool found = false;
  u64 pos = 0;
  for (u64 i = 0; i < ArrayList_len(v->alloc_blocks); ++i) {
    if (((VirtualPtr*)ArrayList_at(v->alloc_blocks, i))->id == virt_ptr) {
      pos = i;
      break;
    }
  }
  if (!found) {
    return false;
  } else {
    *raw_ptr = ((VirtualPtr*)ArrayList_at(v->alloc_blocks, pos))->pos;
    return true;
  }
}

int comp_u64(const void* a, const void* b) {
  u64 c = *((u64*)a);
  u64 d = *((u64*)b);
  if (c > d) return 1;
  if (d > c) return -1;
  return 0;
}

bool VmAllocator_get_min_virt_ptr(VmAllocator *v, u64 *virt_ptr) {
  if (ArrayList_len(v->alloc_blocks) == 0) {
    *virt_ptr = 1 + ((u64)1 << 63);
    return true;
  }
  ArrayList *virt_ptrs = ArrayList_new(sizeof(u64));
  for (u32 i = 0; ArrayList_len(v->alloc_blocks) > i; ++i) {
    const VirtualPtr* x = ArrayList_at(v->alloc_blocks, i);
    if ((x->id >> 63) != 0) ArrayList_push(virt_ptrs, &x->id);
  }
  qsort(ArrayList_internal_mem(virt_ptrs), ArrayList_len(virt_ptrs), sizeof (u64), comp_u64);
  u64 res = *((u64*)ArrayList_at(virt_ptrs, 0));
  for (u64 i = 1; ArrayList_len(virt_ptrs) > i; ++i) {
    const u64* vp = ArrayList_at(virt_ptrs, i);
    if (res + 1 < *vp) {
      *virt_ptr = res;
      ArrayList_free(virt_ptrs);
      return true;
    } else {
      res = *vp;
    }
  }
  if (ArrayList_len(virt_ptrs) != 0) {
    const u64* last = ArrayList_at(virt_ptrs, ArrayList_len(virt_ptrs)-1);
    *virt_ptr = *last + 1;
    ArrayList_free(virt_ptrs);
    return true;
  } else {
    *virt_ptr = 1 + ((u64)1<<63);
    ArrayList_free(virt_ptrs);
    return true;
  }
  *virt_ptr = 1 + ((u64)1<<63);
  ArrayList_free(virt_ptrs);
  return true;
  //static_assert(false, "unimplemented!");
}

int VirtualPtr_comp_pos(const void *a, const void *b) {
  const VirtualPtr* x = a;
  const VirtualPtr* y = b;
  if (x->pos > y->pos) return 1;
  if (x->pos < y->pos) return -1;
  return 0;
}

void VmAllocator_free_pages(VmAllocator *v) {
  f64 mem_usage = ((f64)v->fst_arena->free_ptr - 1.0) / (f64)(v->page_size * v->page_number);
  f64 max_freeable = (f64)(v->page_number - MAX_PAGES);
  f64 free_target = (1.0 - mem_usage) * (f64)v->page_number;
  v->page_number -= (u64)(free_target < max_freeable ? free_target : max_freeable);
  VmArena_resize(v->fst_arena, v->page_size * v->page_number);
}

void VmAllocator_collect_garbage(VmAllocator *v) {
  qsort(v->alloc_blocks, ArrayList_len(v->alloc_blocks), sizeof (VirtualPtr), VirtualPtr_comp_pos);
  VmArena_deinit(v->fst_arena);
  VirtualPtr* curr = NULL;
  for (u64 i = 0; i < ArrayList_len(v->alloc_blocks); ++i) {
    curr = ArrayList_at_mut(v->alloc_blocks, i);
    for (u64 j = 0; j < curr->size; ++j) {
      *((VirtualPtr*)ArrayList_at_mut(v->fst_arena->memory, v->fst_arena->free_ptr + j)) = *((VirtualPtr*)ArrayList_at(v->fst_arena->memory, curr->pos + j));
    }
    curr->pos = v->fst_arena->free_ptr;
    v->fst_arena->free_ptr += curr->size + 1;
  }
  if (v->page_number > MAX_PAGES) {
    VmAllocator_free_pages(v); 
  }
}

u64 VmAllocator_malloc(VmAllocator *v, unsigned long long nbytes) {
  //static_assert(false,"unimplemented");
  u64 main_total = v->fst_arena->total_size;
  u64 main_occupied = v->fst_arena->free_ptr;
  u64 main_free = main_total - main_occupied;
  if (main_free < nbytes) {
    VmAllocator_collect_garbage(v);
    main_total = v->fst_arena->total_size;
    main_occupied = v->fst_arena->free_ptr;
    main_free = main_total - main_occupied;
    while (main_free < nbytes) {
      VmAllocator_add_page(v);
      main_total = v->fst_arena->total_size;
      main_occupied = v->fst_arena->free_ptr;
      main_free = main_total - main_occupied;
    }
  }
  u64 virt_ptr = 0; 
  VmAllocator_get_min_virt_ptr(v, &virt_ptr);
  VirtualPtr* new_block = VirtualPtr_new(virt_ptr, v->fst_arena->free_ptr, nbytes);
  v->fst_arena->free_ptr += nbytes + 1;
  ArrayList_push(v->alloc_blocks, new_block);
  free(new_block);
  return virt_ptr;
}

void VmAllocator_mfree(VmAllocator *v, unsigned long long virt_ptr) {
  VirtualPtr* x = NULL;
  for (u64 i = 0; i < ArrayList_len(v->alloc_blocks); ++i) {
    x = ArrayList_at_mut(v->alloc_blocks, i);
    if (x->id == virt_ptr) {
      ArrayList_pop(v->alloc_blocks, x);
      break;
    }
  }
}

bool VmAllocator_size_of_alloc(VmAllocator *v, u64 virt_ptr, u64* size) {
  bool found = false;
  u64 pos = 0;
  for (u64 i = 0; i < ArrayList_len(v->alloc_blocks); ++i) {
    if (((VirtualPtr*)ArrayList_at(v->alloc_blocks, i))->id == virt_ptr) {
      pos = i;
      break;
    }
  }
  if (!found) {
    return false;
  } else {
    *size = ((VirtualPtr*)ArrayList_at(v->alloc_blocks, pos))->size;
    return true;
  }
}


void VmAllocator_kill(VmAllocator *v) {
  VmArena_free(v->fst_arena);
  ArrayList_free(v->alloc_blocks);
  free(v);
}
