#ifndef MAKINA_LIB_H
#define MAKINA_LIB_H
#include "../types/types.h"
#include "../libArrayList/libArrayList.h"
#include "../VMAlloc/VMAlloc.h"


typedef struct vm VM;
typedef struct nat_wrap Native_Wrapper;
typedef union reg {
  u8  asbytes[8];
  u32 as32[2];
  u64 as64;
} Register;

VM* VM_new();
void VM_free(VM* vm);
u64 VM_load_prog(VM* self, ArrayList* prog);
void VM_resume(VM* self, u64 fatptr);
void VM_push_native(VM* self, Native_Wrapper* nat);
Register *VM_get_reg(VM* self, u8 reg);

void VM_execute(VM* vm); 

#endif 

