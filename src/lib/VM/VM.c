#include "VM.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif 

typedef void (*Native_t)(VM*);
typedef struct vm {
  Register a; 
  Register b;
  Register c;
  Register d;
  Register e;
  Register f;
  Register g;
  Register h;
  u8 flg;
  u64 pc;
  bool hlt;
  // ArrayList *program; // This has been Removed
  ArrayList *natives;
  ArrayList *stack;
  VmAllocator *heap;
} VM;

typedef struct nat_wrap {
  Native_t native;
} Native_Wrapper;

VM* VM_new() {
  VM* result = malloc(sizeof(VM));
  result->a.as64 = 0;
  result->b.as64 = 0;
  result->c.as64 = 0;
  result->d.as64 = 0;
  result->e.as64 = 0;
  result->f.as64 = 0;
  result->g.as64 = 0;
  result->h.as64 = 0;
  result->flg = 0;
  result->pc = 0;
  result->hlt = 0;
  result->natives = ArrayList_new(sizeof(Native_Wrapper));
  result->stack = ArrayList_new(sizeof(u64));
  result->heap = VmAllocator_new(PAGE_SIZE);
  return result;
}

void VM_free(VM *vm) {
  ArrayList_free(vm->natives);
  ArrayList_free(vm->stack);
  VmAllocator_kill(vm->heap);
  free(vm);
}

Native_Wrapper* wrap_native(Native_t native) {
  Native_Wrapper* res = malloc(sizeof (Native_Wrapper));
  res->native = native;
  return res;
}

void VM_push_native(VM *self, Native_Wrapper* nat) {
  ArrayList_push(self->natives, nat);
}

