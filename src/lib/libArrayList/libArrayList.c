#include "libArrayList.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct vec {
  u64   type_size;
  u64   capacity;
  u64   size;
  u8* mem_region;
} ArrayList;

// Creates new ArrayList with each element having alignment bytes. 
ArrayList* ArrayList_new(u32 alignment) {
  ArrayList* res = malloc(sizeof(ArrayList));
  res->type_size = alignment;
  res->size = 0;
  res->capacity = 32;
  res->mem_region = calloc(res->capacity, res->type_size);
  return res;
}

// List, size in bytes, fill if bigger, returning false on failure.
bool ArrayList_resize(ArrayList* list, u64 size, u8 fill) {
  if (list->capacity == size) {
    return true;
  } else if (list->capacity > size) {
    list->mem_region = realloc(list->mem_region, size * list->type_size);
    if (list->mem_region == NULL) return false;
    list->capacity = size;
    if (list->size >= list->capacity) list->size = list->capacity;
    return true;
  } else {
    list->mem_region = realloc(list->mem_region, size * list->type_size);
    if (list->mem_region == NULL) return false;
    u64 n_to_fill = (size - list->capacity) * list->type_size;
    memset(list->mem_region + list->capacity, fill, n_to_fill);
    list->capacity = size;
    return true;
  }
} 

// Const accessor
const void* ArrayList_at(const ArrayList* list, u64 index) {
  return &list->mem_region[index * list->type_size];
}

// Mut accessor
void* ArrayList_at_mut(ArrayList* list, u64 index) {
  return &list->mem_region[index * list->type_size];
}

// Push_back
// It will dereference said void* to the size specified at the creation of the ArrayList.
bool ArrayList_push(ArrayList* list, const void* value) {
  list->size++;
  while (list->size >= list->capacity) {
    if (!ArrayList_resize(list, list->capacity * 3 / 2, 0)) return false;
  } 
  return (memcpy(list->mem_region + (list->size - 1)*list->type_size, value, list->type_size) != NULL); 
}

// Pop_Back
// it will copy the value into the result
bool ArrayList_pop(ArrayList* list, void* result) {
  list->size--;
  if ((i64)list->size < 0) {
    list->size = 0;
    return false;
  }
  return (memcpy(result, list->mem_region + (list->size)*list->type_size, list->type_size) != NULL);
}

u64 ArrayList_len(const ArrayList* const list) {
  return list->size;
}

void ArrayList_free(ArrayList *list) {
  free(list->mem_region);
  free(list);
  return;
}

void ArrayList_dump_mem(ArrayList *list) {
  printf("\n ----- MEM DUMP ----- \n");
  for(u64 i = 0; i < list->capacity; ++i) {
    for(u64 j = 0; j < list->type_size; ++j) {
      printf("%03d ",((char*)(list->mem_region))[i*list->type_size + j]);
    }
    printf("\n");
  }
  printf(" ----- That was a good dump ----- \n");
}


u8* ArrayList_internal_mem(ArrayList* list) {
  return list->mem_region;
}

