#ifndef LIBARRAYLIST_H
#define LIBARRAYLIST_H
#include "../types/types.h" 
#include <stdbool.h>

// ArrayList is a list of bytes.


typedef struct vec ArrayList;

// Creates new ArrayList with each element having alignment bytes. 
ArrayList* ArrayList_new(u32 alignment);

// List, size in bytes, fill if bigger, returning false on failure.
bool ArrayList_resize(ArrayList* list, u64 size, u8 fill); 

// Const accessor
const void* ArrayList_at(const ArrayList* list, u64 index);

// Mut accessor
void* ArrayList_at_mut(ArrayList* list, u64 index);

// Push_back
// It will dereference said void* to the size specified at the creation of the ArrayList.
bool ArrayList_push(ArrayList* list, const void* value);

// Pop_Back
// it will copy the value into the result
bool ArrayList_pop(ArrayList* list, void* result);

u8* ArrayList_internal_mem(ArrayList* list);

u64 ArrayList_len(const ArrayList* const list);

void ArrayList_free(ArrayList* list);

void ArrayList_dump_mem(ArrayList* list);

#endif
